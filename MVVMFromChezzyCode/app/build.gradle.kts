plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt") //kapt means Kotlin Annotation Processing Tool
    kotlin("kapt")
    id("com.google.dagger.hilt.android")//Hilt Dependency
}

android {
    namespace = "com.example.mvvmchezzycode"
    compileSdk = 33

    defaultConfig {
        applicationId = "com.example.mvvmchezzycode"
        minSdk = 19
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

//        javaCompileOptions {
//            annotationProcessorOptions {
//                arguments += mapOf(
//                    "room.schemaLocation" to "$projectDir/schemas",
//                    "room.incremental" to "true"
//                )
//            }
//        }

    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
        freeCompilerArgs = listOf("-Xjvm-default=all")
    }
}

dependencies {

    implementation("androidx.core:core-ktx:1.9.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.9.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")

    //For Hilt
    implementation("com.google.dagger:hilt-android:2.48")
    kapt("com.google.dagger:hilt-android-compiler:2.48")


//    implementation("com.google.dagger:dagger:2.48")
//    kapt("com.google.dagger:dagger-compiler:2.48") //kapt means Kotlin Annotation Processing Tool

    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.5.1")

    val room_version = "2.4.3"
    implementation("androidx.room:room-runtime:$room_version")
    implementation("androidx.room:room-ktx:$room_version")
    kapt("androidx.room:room-compiler:$room_version")

    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.squareup.retrofit2:converter-gson:2.9.0")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.6.4")
//----------------------------------------------------------------------------------------
//    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.5.1")
//
//    val retrofit_version = "2.9.0"
//    implementation("com.squareup.retrofit2:retrofit:$retrofit_version")
//    implementation("com.squareup.retrofit2:converter-gson:$retrofit_version")
//
//
//    val roomVersion = "2.5.2"
//    implementation("androidx.room:room-runtime:$roomVersion")
//    ksp("androidx.room:room-compiler:$roomVersion")
//    ksp("androidx.lifecycle:lifecycle-common:2.6.2")
//
//    val coroutines_version = "1.6.4"
//    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
//    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutines_version")
//
//    testImplementation("androidx.arch.core:core-testing:2.1.0")
//    androidTestImplementation("androidx.arch.core:core-testing:2.1.0")
//    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.6.4")
//    androidTestImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.6.4")
//    androidTestImplementation("com.google.dagger:hilt-android-testing:2.44")
//    kaptAndroidTest("com.google.dagger:hilt-android-compiler:2.44")
//    testImplementation("androidx.test:runner:1.5.1")
//    androidTestImplementation("androidx.test:runner:1.5.1")
//    androidTestImplementation("androidx.test:core:1.5.0")




}
//
//room {
//    schemaDirectory("$projectDir/schemas/")
//}