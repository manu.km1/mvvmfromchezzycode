package com.example.mvvmchezzycode.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.mvvmchezzycode.models.Product


//This our DataBase creation
@Database(entities = [Product::class] , version = 1 , exportSchema = false)
abstract class  FakerDB : RoomDatabase() {

    abstract fun getFakerDAO() : FakerDAO
}