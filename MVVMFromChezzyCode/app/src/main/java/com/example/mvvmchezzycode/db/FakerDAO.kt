package com.example.mvvmchezzycode.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.mvvmchezzycode.models.Product

//This is our Database manuipulation commands
@Dao
interface FakerDAO {

    @Insert
    suspend fun addProducts(product: List<Product>)
    @Query("SELECT * FROM Product")
    fun getProducts() : List<Product>
}