package com.example.mvvmchezzycode.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.mvvmchezzycode.db.FakerDB
import com.example.mvvmchezzycode.models.Product
import com.example.mvvmchezzycode.retrofit.FakerAPI
import javax.inject.Inject

class ProductRepository @Inject constructor(private val fakerAPI: FakerAPI , private val fakerDB: FakerDB) { /* Dagger will provide the FakerAPI object,
                                                                                   because we used Connstructor Injection */

    private val _products = MutableLiveData<List<Product>>() /* stores list of Products and MutableLiveData used because if we change
                                                                data, still we cannot access to publically*/
    val products : LiveData<List<Product>>   /*  LiveData used, cos It is Read only   */
        get() =_products

    suspend fun getProducts(){
        val result = fakerAPI.getProducts()
        //We will expose LiveData here, so our ViewModel can access the data
        if(result.isSuccessful && result.body() != null){
            fakerDB.getFakerDAO().addProducts(result.body()!!)
                _products.postValue(result.body())
        }
    }
}