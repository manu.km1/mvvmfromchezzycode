package com.example.mvvmchezzycode.retrofit

import com.example.mvvmchezzycode.models.Product
import retrofit2.Response
import retrofit2.http.GET

interface FakerAPI {
    @GET("products")
   suspend fun getProducts() : Response<List<Product>>

}